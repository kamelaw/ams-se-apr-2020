console.log("Avengers Management System activated")

let roster = [
    {
        heroName: "Captain America",
        specialty: "shields",
        damage: 132,
        secretIdentity: "Steve",
    },
    {
        heroName: "Hulk",
        damage: 1500,
        specialty: "smash",
        secretIdentity: "Bruce",
    },
    {
        heroName: "Iron Man",
        secretIdentity: "Tony",
        specialty: "suits",
        damage: 200
    },
    {
        heroName: "Thor",
        specialty: "suits",
        damage: 135
    },
    {
        heroName: "Black Widow",
        secretIdentity: "Natasha",
        specialty: "butt-kicking",
        damage: 40
    },
    {
        heroName: "Hawkeye",
        secretIdentity: "Clint",
        specialty: "archery",
        damage: 0.1
    },
    {
        heroName: "Black Panther",
        secretIdentity: "T'challa",
        specialty: "claws",
        damage: 50
    },
    {
        heroName: "Scarlet Witch",
        secretIdentity: "Wanda",
        damage: 35
    },
    {
        heroName: "Antman",
        secretIdentity: "Scott",
        specialty: "shrinking"
    }
]

let createHeroPropertiesPara = function (heroObj) {
    let markup = "<ul>"
    
    if (heroObj.secretIdentity) {
        markup += "<li>Secret Identity: " + heroObj.secretIdentity + "</li>"
    }
    if (heroObj.specialty) {
        markup += "<li>Specialty: " + heroObj.specialty + "</li>"
    }
    if (heroObj.damage) {
        markup += "<li>Property Damage: $ " + heroObj.damage + " million</li>"
    }

    markup += "</ul>"
    return markup
}

let createHeroHeader = function (name) {
    let heroHeader = document.createElement("h2")
    let textNode = document.createTextNode(name)
    // Put the text into the <h2>
    heroHeader.appendChild(textNode)
    return heroHeader
}

let createHeroLi = function (heroObj) {
    let heroName = heroObj.heroName
    console.log(heroName)

    let listItem = document.createElement("li")
    listItem.appendChild(createHeroHeader(heroName))
    listItem.innerHTML = listItem.innerHTML
        + createHeroPropertiesPara(heroObj)
    
    return listItem
}

// Get a reference to the <ul> so that we can put
// things into it later
let listEl = document.getElementById("avengers")

for (let heroIndex=0; heroIndex<roster.length; heroIndex++) {
    listEl.appendChild(createHeroLi(roster[heroIndex]))
}